import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

import { Group } from './../../models/group/group.model';
import {Observable} from 'rxjs/Rx';
import {map} from 'rxjs/internal/operators';

@Injectable()
export class GroupsListService {
	private groupsListRef = this.db.list<Group>('groups-list');
	public all_groups: Observable<Group[]>;

	constructor(private db: AngularFireDatabase){
		this.all_groups = this.getGroupsList()
			.snapshotChanges().pipe(
				map(changes => {
					return changes.map(c => ({
						key: c.payload.key,
						...c.payload.val()
					}));
				})
			);
	}


	getGroupsList(){
		return this.groupsListRef;
	}


	addGroup(group: Group){
		return this.groupsListRef.push(group);
	}

	editGroup(group: Group){
		return this.groupsListRef.update(group.key, group);
	}

	removeGroup(group: Group){
		return this.groupsListRef.remove(group.key);
	}
}