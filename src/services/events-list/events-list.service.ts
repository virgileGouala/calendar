import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

import { Event } from './../../models/event/event.model';

@Injectable()
export class EventsListService {
	private eventsListRef = this.db.list<Event>('events-list');

	constructor(private db: AngularFireDatabase){}


	getEventsList(){
		return this.eventsListRef;
	}


	addEvent(event: Event){
		return this.eventsListRef.push(event);
	}

	editEvent(event: Event){
		return this.eventsListRef.update(event.key, event);
	}

	removeEvent(event: Event){
		return this.eventsListRef.remove(event.key);
	}
}