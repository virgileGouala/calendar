import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '@firebase/auth-types';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import {Group} from '../../models/group/group.model';
import {GroupsListService} from '../groups-list/groups-list.service';
import {App} from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
@Injectable()
export class UserService{

	current_user: any;
	all_groups: Observable<Group[]>;
	user_groups: Array<{id: string, name: string}> = [];
	private UserListRef = this.db.list<User>('users-list');
		constructor(private db: AngularFireDatabase, private fireAuth: AngularFireAuth,
				private group_service: GroupsListService,
				private app: App){
		this.current_user = this.fireAuth.authState;
		this.all_groups = this.group_service.getGroupsList()
			.snapshotChanges().pipe(
				map(changes => {
					return changes.map(c => ({
						key: c.payload.key,
						...c.payload.val()
					}));
				})
			);
		this.getUser().subscribe((user: any) => {
			if (user !== null && user !== undefined)
				this.getUserGroupsById(user.uid);
		})
	}

	getUser(): Observable<User> {
		return this.current_user.pipe(take(1));
	}

	isLoggedIn(): Observable<boolean> {
		return this.current_user.pipe(
			take(1),
			map(authState => !!authState)
		);
	}

	logUser() {
		this.getUser().subscribe(user => console.log(user));
	}

	/**
	 * Logout the current_user and rediret to signInPage
	 * @author SN
	 * @update 22-10-2018
	 */
	logout() {
		this.fireAuth.auth.signOut().then(() => {
			this.app.getActiveNav().setRoot('SignInPage');
		}, (error) => {
			console.log(error);
		});
	}

	/**
	 * Get the groups where current_user is admin or a member
	 * @param id: string
	 * @author SN
	 * @update 21-20-2018
	 */
	getUserGroupsById(id: string): void {
		this.all_groups.subscribe((groups: Array<Group>) => {
			for (let group of groups) {
				if ((group.users !== undefined && group.users.indexOf(id) > -1)
					|| group.idUserAdmin === id) {
					this.user_groups.push({
						id: group.key,
						name: group.name
					});
				}
			}
		});

	}

	addUser(user){
		return this.UserListRef.push(user);
	}

	listAllUsers(nextPageToken){
		// List batch of users, 1000 at a time.
		console.log("load users Start");
		let data : any = [];
		return this.db.database.ref('users-list')
		.once('value');

	}
	// Start listing users from the beginning, 1000 at a time.
	// listAllUsers();

}
