export interface Event{
	key?: string;
	userId: string;
	date?: string;
	dateOfCreation: string;
	dateOfLastModification: string;
	name: string;
	description: string;
	visibility: boolean;
	groupId: string;
}