export interface Group {
	key?: string;
	idUserAdmin: string;
	name: string;
	dateOfCreation: string;
	visibility: boolean;
	users: Array<any>;
}