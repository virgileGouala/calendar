import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { User } from '../../models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from './../../services/user/user.service';
/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
     selector: 'page-sign-up',
     templateUrl: 'sign-up.html',
 })
 export class SignUpPage {

     user = {} as User;

     constructor(private fireAuth: AngularFireAuth, private toast: ToastController, public navCtrl: NavController, public navParams: NavParams, private userService: UserService) {
     }

     async signUp(user: User) {

         try{
             const info = this.fireAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
			 this.userService.addUser(user);
             if(info){

                 this.navCtrl.setRoot('SignInPage')
             }
         } catch(e){
             this.toast.create({
                 message: "All fields are required! Password must be at least 6 characters long.",
                 duration: 4000,
                 cssClass: "error"
             }).present();
         }

     }

 }
