import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Event } from './../../models/event/event.model';
import { EventsListService } from './../../services/events-list/events-list.service';
import { ToastService } from './../../services/toast/toast.service';
import {UserService} from '../../services/user/user.service';

@IonicPage()
@Component({
	selector: 'page-edit-event',
	templateUrl: 'edit-event.html',
})
export class EditEventPage {

	event: Event;
	eventName: string;
	timeZone: number = new Date().getTimezoneOffset() * 60000;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private events: EventsListService,
		private toast: ToastService,
		public user: UserService
		){}

	ionViewWillLoad() {
		this.event = this.navParams.get('event');
		if (this.event !== undefined)
			this.eventName = this.event.name;
	}

	saveEvent(event){
		this.event.dateOfLastModification  = (new Date(Date.now() - this.timeZone)).toISOString().slice(0, -1).slice(0,16);
		this.events.editEvent(event).then(ref=>{
			this.toast.show(`${event.name} saved!`);
			this.navCtrl.setRoot('HomePage');
		})
	}

	removeEvent(event){
		this.events.removeEvent(event).then(()=>{
			this.toast.show(`${event.name} Removed !`);
			this.navCtrl.setRoot('HomePage');
		})
	}
}
