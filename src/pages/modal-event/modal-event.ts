import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {GroupsListService} from '../../services/groups-list/groups-list.service';
import {Group} from '../../models/group/group.model';

/**
 * Generated class for the ModalEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-event',
  templateUrl: 'modal-event.html',
})
export class ModalEventPage {

  event: Event;
  group_name: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController, public group_service: GroupsListService) {
    this.event = this.navParams.get('event');
    this.getGroupName(this.event['groupId']);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalEventPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  /**
   * Set the group name according to the key send to the modal
   * @param key
   * @author SN
   * @update 22-10-2018
   */
  getGroupName(key: string) {
    this.group_service.all_groups.subscribe((groups: Array<Group>) => {
      for (let group of groups) {
        if (group.key === key)
          this.group_name = group.name;
      }
    })
  }

}
