import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Event } from './../../models/event/event.model';
import { EventsListService } from './../../services/events-list/events-list.service';
import { ToastService } from './../../services/toast/toast.service';
import { UserService } from './../../services/user/user.service';

/**
* Generated class for the AddEventPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
	selector: 'page-add-event',
	templateUrl: 'add-event.html',
})
export class AddEventPage {

	timeZone: number = new Date().getTimezoneOffset() * 60000;
	event: Event = {
		userId : "",
		date : "",
		dateOfCreation : "",
		dateOfLastModification : "",
		name : "Lorem Ipsum",
		description : "Lorem Ipsum Dolor",
		visibility : false,
		groupId: ""
	}

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private events: EventsListService,
		private toast: ToastService,
		private user: UserService)
	{
		this.event.date = (new Date(Date.now() - this.timeZone)).toISOString().slice(0, -1).slice(0,16);
		this.event.dateOfLastModification = this.event.date;
		this.event.dateOfCreation = this.event.date;
		this.user.getUser().subscribe((user: any) => {
			if (user !== null && user !== undefined)
				this.event.userId = user['uid'];
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AddEventPage');
	}

	/**
	 * Add new event object on the database
	 * and redirect to the homepage (events list)
	 * @param event
	 * @author VG
	 * @update 21-10-2018
	 */
	addEvent(event: Event){
		console.log(event);
		this.events.addEvent(event).then(ref=>{
			this.toast.show(`${event.name} added!`);
			this.navCtrl.setRoot('HomePage', { key: ref.key });
		})
	}
}
