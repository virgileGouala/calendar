import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { User } from '../../models/user';
import { AngularFireAuth } from 'angularfire2/auth';


@IonicPage()
@Component({
    selector: 'page-sign-in',
    templateUrl: 'sign-in.html',
})
export class SignInPage {

    user = {} as User;
    constructor(private fireAuth: AngularFireAuth, private toast: ToastController, public navCtrl: NavController, public navParams: NavParams) {
    }

    async signIn(user: User){
        try{
            const info = this.fireAuth.auth.signInWithEmailAndPassword(user.email, user.password);
            if(info){
                console.log(user)
                this.navCtrl.setRoot('HomePage');
            }
        } catch(e){
            this.toast.create({
                message: "Invalid email or password",
                duration: 3000,
                cssClass: "error"
            }).present();
        }
    }

    signUp(){
        this.navCtrl.push('SignUpPage');
    }
}
