import { Component, OnInit } from '@angular/core';
import {NavController, IonicPage, ModalController} from 'ionic-angular';

import { Event } from './../../models/event/event.model';
import { EventsListService } from './../../services/events-list/events-list.service';
import { UserService } from './../../services/user/user.service';

import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import {ModalEventPage} from '../modal-event/modal-event';

@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage implements OnInit{

	eventsList$: Observable<Event[]>;
	user_id: string;
	displayedEvents: Array<Event> =[];

	constructor(public navCtrl: NavController,
				private events: EventsListService,
				public user: UserService,
				public modalCtrl: ModalController) {

		this.eventsList$ = this.events
		.getEventsList()
		.snapshotChanges().pipe(
			map(changes => {
				return changes.map(c => ({
					key: c.payload.key,
					...c.payload.val()
				}));
			})
		);
		this.getDisplayableEvents();
	};

	ngOnInit(){
		this.user.getUser().subscribe((user: any) => {
			if (user !== null && user !== undefined)
				this.user_id = user['uid'];
		});
		console.log(this.user.user_groups);
	}

	/**
	 * Filter all the events by group and current_user
	 * @author SN
	 * @update 21-10-2018
	 */
	getDisplayableEvents() {
		this.eventsList$.subscribe((events: Array<Event>) => {
			for (let event of events) {
				if (event.userId === this.user_id)
					this.displayedEvents.push(event);
				else {
					for (let group of this.user.user_groups) {
						if (group.id === event.groupId)
							this.displayedEvents.push(event);
					}
				}
			}
		})
	}

	openModal(event: object) {
		const modal = this.modalCtrl.create(ModalEventPage, event);
    	modal.present();

	}
}