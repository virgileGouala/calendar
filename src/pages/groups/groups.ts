import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Group } from './../../models/group/group.model';
import { GroupsListService } from './../../services/groups-list/groups-list.service';
import { ToastService } from './../../services/toast/toast.service';
import { UserService } from './../../services/user/user.service';

import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@IonicPage()
@Component({
	selector: 'page-groups',
	templateUrl: 'groups.html',
})
export class GroupsPage {

	groupsList$: Observable<Group[]>;
	user_id: string;
	displayedGroups: Array<Group> = [];
	constructor(public navCtrl: NavController, private groups: GroupsListService, private user: UserService) {

		this.groupsList$ = this.groups
		.getGroupsList()
		.snapshotChanges().pipe(
			map(changes => {
				return changes.map(c => ({
					key: c.payload.key,
					...c.payload.val()
				}));
			})
			);
		this.getDisplayableGroups();
	};

	ngOnInit(){
		this.user.getUser().subscribe((user: any) => {
			this.user_id = user['uid'];
		});
	}

	/**
	 * Filter all the groups by visibility and members
	 * @author SN
	 * @update 21-10-2018
	 */
	getDisplayableGroups(): void {
		this.groupsList$.subscribe((groups: Array<Group>) => {
			for (let group of groups) {
				if (group.visibility === true) {
					this.displayedGroups.push(group);
				}
				else {
					if (group.idUserAdmin === this.user_id
						|| (group.users !== undefined && group.users.indexOf(this.user_id) > -1))
						this.displayedGroups.push(group);
				}
			}
		});
	}

}
