import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Group } from './../../models/group/group.model';
import { GroupsListService } from './../../services/groups-list/groups-list.service';
import { ToastService } from './../../services/toast/toast.service';
import { UserService } from './../../services/user/user.service';
import { User, UserCredential } from '@firebase/auth-types';
/**
* Generated class for the AddGroupPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
	selector: 'page-add-group',
	templateUrl: 'add-group.html',
})
export class AddGroupPage {

	timeZone: number = new Date().getTimezoneOffset() * 60000;

	group: Group = {
		idUserAdmin: "",
		name: "",
		dateOfCreation: "",
		visibility: false,
		users: []
	};
	localEmail : string;

	users : Array<User> = [];
	items = [];
	searchQuery: string = '';

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private groups: GroupsListService,
		private toast: ToastService,
		private user: UserService)
		{
			this.group.dateOfCreation = (new Date(Date.now() - this.timeZone)).toISOString().slice(0, -1).slice(0,16);
			this.user.getUser().subscribe((user: any) => {
				this.group.idUserAdmin = user['uid'];
				this.localEmail = user['email'];
			})
		}
		ionViewDidEnter(){
			let data;
			this.user.listAllUsers(0).then(snapshot => snapshot.val())
			.then(users => {
				let evilResponseProps = Object.keys(users);
				console.log(evilResponseProps);

				console.log(this.group.idUserAdmin);
				for (let prop of evilResponseProps) {
					console.log(prop);

					if(users[prop].email != this.localEmail) {
						this.users.push(users[prop]);
					}
				}
				this.initializeItems();
				console.log(this.users);
			});
		}
		ionViewDidLoad() {
			console.log('ionViewDidLoad AddGroupPage');
		}

		addGroup(group: Group){
			this.groups.addGroup(group).then(ref=>{
				this.toast.show(`${group.name} added!`);
				this.navCtrl.setRoot('GroupsPage', { key: ref.key });
			})
		}
		initializeItems(){
			this.items = this.users;
		}
		getItems(ev: any) {
			// Reset items back to all of the items
			this.initializeItems();

			// set val to the value of the searchbar
			let val = ev.target.value;

			// if the value is an empty string don't filter the items
			if (val && val.trim() != '') {
			this.items = this.items.filter((item) => {
				return (item.email.toLowerCase().indexOf(val.toLowerCase()) > -1);
			});
		}
	}
	addToGroup(item){
		if(this.localEmail != item.email) {
			this.group.users.push(item);
		}
		console.log(this.localEmail);
	}
	removeFromGroup(item){
		const index: number = this.group.users.indexOf(item);
	    if (index !== -1) {
	        this.group.users.splice(index, 1);
	    }
	}


}
