import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from './../../services/user/user.service';
import { Group } from './../../models/group/group.model';
import { GroupsListService } from './../../services/groups-list/groups-list.service';
import { ToastService } from './../../services/toast/toast.service';
import { User, UserCredential } from '@firebase/auth-types';
@IonicPage()
@Component({
	selector: 'page-edit-group',
	templateUrl: 'edit-group.html',
})
export class EditGroupPage {

	group: Group;
	groupName: string;
	timeZone: number = new Date().getTimezoneOffset() * 60000;
	users : Array<User> = [];
	items = [];
	searchQuery: string = '';
	localEmail: string = '';
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private groups: GroupsListService,
		private toast: ToastService,
		private user: UserService
		){
			this.user.getUser().subscribe((user: any) => {
				this.localEmail = user['email'];
			})
		}

	ionViewWillLoad() {
		this.group = this.navParams.get('group');
		if (this.group !== undefined)
			this.groupName = this.group.name;
	}
	ionViewDidEnter(){
			let data;
			this.user.listAllUsers(0).then(snapshot => snapshot.val())
			.then(users => {
				let evilResponseProps = Object.keys(users);
				console.log(evilResponseProps);
				for (let prop of evilResponseProps) {
					if(users[prop].email != this.localEmail) {
						this.users.push(users[prop]);
					}
				}
				this.initializeItems();
				console.log(this.users);
			});
		}


	saveGroup(group){
		this.groups.editGroup(group).then(ref=>{
			this.toast.show(`${group.name} saved!`);
			this.navCtrl.setRoot('GroupsPage');
		})
	}

	removeGroup(group){
		this.groups.removeGroup(group).then(()=>{
			this.toast.show(`${group.name} Removed !`);
			this.navCtrl.setRoot('GroupsPage');
		})
	}
	initializeItems(){
		this.items = this.users;
	}
	getItems(ev: any) {
		// Reset items back to all of the items
		this.initializeItems();

		// set val to the value of the searchbar
		let val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
		this.items = this.items.filter((item) => {
			return (item.email.toLowerCase().indexOf(val.toLowerCase()) > -1);
		});
	}
}
addToGroup(item){
	if(this.localEmail != item.email) {
		this.group.users.push(item);
	}
}
removeFromGroup(item){
	const index: number = this.group.users.indexOf(item);
	if (index !== -1) {
		this.group.users.splice(index, 1);
	}
}
}
