import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {UserService} from '../services/user/user.service';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
  rootPage: string = 'SignInPage';

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private user: UserService) {
    this.user.getUser().subscribe((current_user: any) => {
      if (current_user !== null && current_user !== undefined)
        this.rootPage = 'HomePage';
      else this.rootPage = 'SignInPage';
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

